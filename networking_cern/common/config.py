# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2013 Somebody
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_config import cfg


cern_network_opts = [
    cfg.StrOpt('landb_hostname',
               default='', secret=True,
               help='landb hostname'),
    cfg.StrOpt('landb_port',
               default='443', secret=True,
               help='landb port'),
    cfg.StrOpt('landb_protocol',
               default='https', secret=True,
               help='landb protocol'),
    cfg.StrOpt('landb_username',
               default='', secret=True,
               help='landb username'),
    cfg.StrOpt('landb_password',
               default='', secret=True,
               help='landb password')
]

cern_metadata_opts = [
    cfg.StrOpt('metadata_host',
               default='127.0.0.1', secret=False,
               help='Host of the metadata server for this hypervisor'),
    cfg.StrOpt('metadata_port',
               default='8775', secret=False,
               help='Port of metadata server for this hypervisor')
]

cfg.CONF.register_opts(cern_network_opts, 'CERN')
cfg.CONF.register_opts(cern_metadata_opts, 'CERN')
