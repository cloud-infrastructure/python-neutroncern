#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from neutron.db import model_base
from neutron.db import models_v2

import sqlalchemy as sa
from sqlalchemy import orm


class Cluster(model_base.BASEV2, models_v2.HasId, models_v2.HasTenant):

    __tablename__ = 'cern_clusters'

    name = sa.Column(sa.String(255), unique=True, nullable=False)


class SubnetCluster(model_base.BASEV2):

    __tablename__ = 'cern_subnet_clusters'

    subnet_id = sa.Column(sa.String(36),
                          sa.ForeignKey('subnets.id'),
                          primary_key=True, unique=True)
    cluster_id = sa.Column(sa.String(36),
                           sa.ForeignKey('cern_clusters.id'),
                           primary_key=True)

    subnets = orm.relationship(
        models_v2.Subnet,
        backref=orm.backref("cluster", lazy='joined',
                            cascade="all, delete-orphan", uselist=False))

    cluster = orm.relationship(
        Cluster,
        backref=orm.backref("subnets", lazy='joined',
                            cascade="all, delete-orphan", uselist=True))
